// APP
const nameApp = 'Cms Templates';
const logoutText = 'Logout';
const cancelText = 'Cancel';

// LOGIN PAGE
const loginTxt = 'Login';
const welcomeTxt = 'Welcome back to the admin panel.';
const labelEmailTxt = 'Email';
const labelPwdText = 'Password';
const hintTextEmail = 'devtree@gmail.com';
const hintTextPwd = 'Tree#dev03@';
const rememberMeTxt = 'Remember me';
const forgotPwdTxt = 'Forgot password?';
const credentialText = 'Do not have admin credentials?';
const reqCredentialTxt = 'Request Credentials!';
const emailRequired = 'Email is required';
const emailValid = 'Email is not valid';
const passwordRequired = 'Password is required';
const passwordValid =
    'Password must be at least 8 characters and include lowercase, uppercase, numbers, and special characters';

// MENU ITEM
const templatesCase = 'Templates';
const newTemplateCase = 'New Template';
const chartCase = 'Chart';
const usersCase = 'Users';
const accountCase = 'Account';
const settingsCase = 'Settings';

// ERROR PAGE
const notFoundText = '404 Page Not Found';
const errNotFoundText = "Sorry, this page isn't available.";
const suggestNotFoundText = 'Go back to home page';
const errorException = 'Something went wrong';
const errorLoadTemplates = 'Error occurred while loading the templates list';

// TEMPLATES PAGE
const idTemplate = 'ID';
const nameTemplate = 'Name';
const typeTemplate = 'Type';
const jsonDataTemplate = 'Json data';
const hintTextSearch = 'Search';
const filterByText = 'Filter by: ';
const deleteTemplateTitle = 'Delete template';
const deleteTemplateDesc =
    'Are you sure that you want to delete this template?';
const deleteManyTemplate =
    'Are you sure that you want to delete templates selected?';
const addTemplateTitle = 'Create new template';
const addTemplateAction = 'Publish';
const editTemplateAction = 'Save';
const newTemplate = 'New template';
const visibleType = 'Visible';
const addType = 'Add';
const editType = 'Edit';
const deleteType = 'Delete';
const idRequired = 'ID is required';
const idValid = 'ID must be a number';
const nameRequired = 'Name is required';
const typeRequired = 'Type is required';
const addSuccess = 'Template created';
const deleteSuccess = 'Template deleted';
const updateSuccess = 'Template updated';
const noTemplates = 'No templates';
const detailTemplateTitle = 'Details template';
const freeType = 'Free';
const mediumType = 'Medium';
const masterType = 'Master';
