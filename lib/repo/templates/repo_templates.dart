import 'dart:convert';

import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/data/data.dart';
import 'package:cms_templates/models/template_model.dart';
import 'package:hive/hive.dart';

// [092301TREE]
class TemplateRepository {
  static const int perPage = 10;
  final box = Hive.box<String>('templates');

  // get all templates
  Future<void> getAllTemplates() async {
    try {
      final jsonData = DataApp.templates['templates'] ?? [];
      final data =
          jsonData.map((t) => TemplateModel.fromJson(t)).toList().reversed;
      saveTemplatesToBox(data.toList());
    } catch (e) {
      throw Exception(errorException);
    }
  }

  // get and convert json to list object
  List<TemplateModel> getListConverted() {
    // get string json from box
    final templateListJson = box.get('templateList') as String;
    // convert to list object
    List<TemplateModel> templateList = (jsonDecode(templateListJson) as List)
        .map((item) => TemplateModel.fromJson(item))
        .toList();
    return templateList;
  }

  // save templates to box
  Future<void> saveTemplatesToBox(List<TemplateModel> templates) async {
    final updatedTemplateListJson = jsonEncode(templates);
    await box.put('templateList', updatedTemplateListJson);
  }

  // calculate total pages
  int calculateTotalPage() {
    // get list converted
    final templateList = getListConverted();
    final totalPages = (templateList.length / perPage).ceil();
    return totalPages;
  }

  // auto increase id plus plus
  int autoIncrementId() {
    // get list converted
    final templateList = getListConverted();
    // id increment
    int id = 1;
    if (templateList.isEmpty) {
      return id; // default start id
    }
    return templateList.first.id + 1;
  }

  // get one template by id
  TemplateModel getOneTemplate(int id) {
    // get list converted
    final templateList = getListConverted();
    return templateList.firstWhere((item) => item.id == id);
  }

  // get templates from box
  Future<List<TemplateModel>> getTemplatesFromBox(int currentPage,
      [List<TemplateModel>? templates]) async {
    final startIndex = (currentPage - 1) * perPage; // store first item of page
    int endIndex = startIndex + perPage; // store last item of page
    // get lis string from box
    final templateListJson = box.get('templateList') as String;
    // convert to list object
    final List<dynamic> templateList = jsonDecode(templateListJson);
    // check is last page, update endIndex to get all items con lai
    if (currentPage == calculateTotalPage()) {
      endIndex = templateList.length;
    }
    // paginate
    // paginate for search results

    // paginate for list in box
    final List<TemplateModel> values = templateList
        .sublist(startIndex, endIndex)
        .map((item) => TemplateModel.fromJson(item))
        .toList();
    return values;
  }

  // add new template to box
  Future<void> addOrEditTemplateOfBox(
      TemplateModel template, String type) async {
    // get list converted
    final templateList = getListConverted();
    // handle add template
    if (type == addType) {
      templateList.insert(0, template);
    }
    // handle edit template
    if (type == editType) {
      final index = templateList.indexWhere((t) => t.id == template.id);
      if (index != -1) {
        templateList[index] = template;
      }
    }
    // save to box
    await saveTemplatesToBox(templateList);
  }

  //  delete one, many template box
  Future<void> deleteTemplate(List<TemplateModel> templates) async {
    // get list converted
    final templateList = getListConverted();
    // remove template from list
    for (int i = 0; i < templates.length; i++) {
      // remove the template with the specified templateId
      templateList.removeWhere((template) => template.id == templates[i].id);
    }
    // save to box
    await saveTemplatesToBox(templateList);
  }

  // delete many templates
  Future<void> deleteTemplates(
    List<TemplateModel> templates,
  ) async {
    for (var template in templates) {
      await box.delete(template.id);
    }
  }

  // delete all templates
  Future<void> deleteAllTemplates() async {
    await box.delete('templateList');
    await box.close();
  }

  // search templates
  Future<List<TemplateModel>> searchTemplates(String keyword) async {
    // get list converted
    final templateList = getListConverted();
    // find
    final results = templateList
        .where((item) => item.name.toLowerCase().contains(keyword))
        .toList();
    return results;
  }
}
