import 'package:cms_templates/constants/routes_text.dart';
import 'package:cms_templates/views/admin/home_page.dart';
import 'package:cms_templates/views/admin/login_page.dart';
import 'package:cms_templates/views/admin/not_found_page.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

GoRouter initRoutes() {
  final router = GoRouter(
    initialLocation: pathHomePage,
    errorPageBuilder: (context, state) => const MaterialPage(
      child: NotFoundPage(),
    ),
    routes: [
      GoRoute(
        name: nameHomePage,
        path: pathHomePage,
        builder: (context, state) {
          // return BlocBuilder<AuthenticationBloc, AuthenticationState>(
          //   builder: (context, state) {
          //     if (state is AuthenticatedState) {
          //       return const HomePage();
          //     } else if (state is UnAuthenticatedState) {
          //       return const LoginPage();
          //       // return Container();
          //     } else {
          //       throw Exception(errorException);
          //     }
          //   },
          // );
          return const HomePage();
        },
      ),
      GoRoute(
        name: nameLoginPage,
        path: pathLoginPage,
        builder: (context, state) => const LoginPage(),
      ),
    ],
  );
  return router;
}
