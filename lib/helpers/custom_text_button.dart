import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/helpers/custom_text.dart';
import 'package:flutter/material.dart';

/* [092305TREE]
Custom simple text button to reuse everywhere
*/
Widget customTextBtn(String title, Function submitButton) {
  // function get color button
  Color getBgColor() {
    switch (title) {
      case addTemplateAction:
        return kTealColor;
      case editTemplateAction:
        return kTealColor;
      case editType:
        return kTealColor;
      case deleteType:
        return kRedColor;
      default:
        return kGrayColor;
    }
  }

  return TextButton(
    style: ButtonStyle(
      backgroundColor: MaterialStatePropertyAll(getBgColor()),
    ),
    onPressed: () {
      submitButton();
    },
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: customText(title, smallSize, textNormal, kWhiteColor),
    ),
  );
}
