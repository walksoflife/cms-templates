import 'package:flutter/material.dart';

/* [092301TREE]
Custom simple text to reuse everywhere
*/
Widget customText(
    String text, double fontSize, FontWeight fontWeight, Color color) {
  return Text(
    text,
    style: TextStyle(fontSize: fontSize, fontWeight: fontWeight, color: color),
  );
}
