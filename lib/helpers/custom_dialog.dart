import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/helpers/custom_text.dart';
import 'package:flutter/material.dart';

/* [092301TREE]
1. This function receives parameters: context, title, action, description, handleTask
- context: where the function is called
- title: the title of the alert dialog
- action: the name of button to confirm alert
- description: the description for the alert dialog
- handleTask: the function is called when confirm the alert dialog
*/

void customDialog(BuildContext context, String title, String action,
    String description, Function handleTask) {
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(title),
        content: Text(description),
        actions: [
          // button cancel
          TextButton(
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(kGrayColor),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: customText(cancelText, smallSize, textNormal, kWhiteColor),
            ),
          ),
          // button confirm
          TextButton(
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(kRedColor),
            ),
            onPressed: () {
              handleTask();
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: customText(action, smallSize, textNormal, kWhiteColor),
            ),
          ),
        ],
      );
    },
  );
}
