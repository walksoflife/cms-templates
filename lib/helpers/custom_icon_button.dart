import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:flutter/material.dart';

/* [092305TREE]
Custom simple text button to reuse everywhere
*/
Widget customIconBtn(String title, Function submitButton) {
  // function get color button
  Color getBgColor() {
    switch (title) {
      case visibleType:
        return kBlackColor;
      case addType:
        return kBlueColor;
      case editType:
        return kTealColor;
      case deleteType:
        return kRedColor;
      default:
        return kGrayColor;
    }
  }

  // function get icon button
  IconData getIcon() {
    switch (title) {
      case visibleType:
        return Icons.visibility;
      case addType:
        return Icons.add;
      case editType:
        return Icons.edit;
      case deleteType:
        return Icons.delete;
      default:
        return Icons.close; // default fake icon
    }
  }

  return IconButton(
      onPressed: () {
        submitButton();
      },
      icon: Icon(getIcon(), size: 20, color: getBgColor()));
}
