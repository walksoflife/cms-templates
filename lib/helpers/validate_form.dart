import 'package:cms_templates/constants/app_text.dart';

/* [092301TREE]
This class is used to validate input of form login, form create template
*/
class Validator {
  // check input is a valid email
  static bool isValidEmail(String value) {
    final emailRegExp = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    return emailRegExp.hasMatch(value);
  }

  // check input is a valid password
  static bool isValidPassword(String value) {
    final passwordRegExp =
        RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\><*~]).{8,}');
    return passwordRegExp.hasMatch(value);
  }

  // display error if email is not valid
  static String? validateEmail(String? value) {
    if (value == null || value.isEmpty) {
      return emailRequired;
    } else if (!isValidEmail(value)) {
      return emailValid;
    }
    return null;
  }

  // display error if password is not valid
  static String? validatePassword(String? value) {
    if (value == null || value.isEmpty) {
      return passwordRequired;
    } else if (!isValidPassword(value)) {
      return passwordValid;
    }
    return null;
  }

  // display error if id template is not valid
  static String? validateIdTemplate(String? value) {
    if (value == null || value.isEmpty) {
      return idRequired;
    }
    final intResult = int.tryParse(value);
    if (intResult == null) {
      return idValid;
    }

    return null;
  }

  // display error if name template is not valid
  static String? validateNameTemplate(String? value) {
    if (value == null || value.isEmpty) {
      return nameRequired;
    }
    return null;
  }

  // display error if type template is not valid
  static String? validateTypeTemplate(String? value) {
    if (value == null || value.isEmpty) {
      return typeRequired;
    }
    return null;
  }
}
