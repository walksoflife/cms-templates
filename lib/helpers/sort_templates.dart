import 'package:cms_templates/models/template_model.dart';

/* [092301TREE]
This function is used to sort templates by id template, includes parameters: 
- list: the list of templates
- isSortAsc: check template is sort ascending ?
*/
void sortTemplatesById(List<TemplateModel> list, bool isSortAsc) {
  if (isSortAsc) {
    list.sort((a, b) => a.id.compareTo(b.id));
  } else {
    list.sort((a, b) => b.id.compareTo(a.id));
  }
}

/* [092301TREE]
This function is used to sort templates by name template, includes parameters: 
- list: the list of templates
- isSortAsc: check template is sort ascending ?
*/
void sortTemplatesByName(List<TemplateModel> list, bool isSortAsc) {
  if (isSortAsc) {
    list.sort((a, b) => a.name.compareTo(b.name));
  } else {
    list.sort((a, b) => b.name.compareTo(a.name));
  }
}
