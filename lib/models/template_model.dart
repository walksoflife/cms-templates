import 'package:hive/hive.dart';

// declare code generator
part 'template_model.g.dart';

/* [092301TREE]
command : flutter packages pub run build_runner build
- run command to generate adapter automatically, because this app store list data
*/

@HiveType(typeId: 0) // typeId is identify
class TemplateModel extends HiveObject {
  @HiveField(0)
  final int id;

  @HiveField(1)
  final String name;

  @HiveField(2)
  final String type;

  TemplateModel({required this.id, required this.name, required this.type});

  factory TemplateModel.fromJson(Map<String, dynamic> json) {
    return TemplateModel(
      id: json["id"],
      name: json["name"],
      type: json["type"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'type': type,
    };
  }
}
