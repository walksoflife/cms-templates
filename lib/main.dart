import 'dart:convert';

import 'package:cms_templates/bloc_cubit/auth/auth_bloc.dart';
import 'package:cms_templates/helpers/config_loading.dart';
import 'package:cms_templates/models/template_model.dart';
import 'package:flutter/material.dart';
import 'package:cms_templates/routes/route_config.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive_flutter/hive_flutter.dart';

// [092301TREE]

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // init hive storage
  await Hive.initFlutter();
  // declare this line to allow box can store list object data
  Hive.registerAdapter<TemplateModel>(TemplateModelAdapter());
  // open box with name is 'templates' to store templates
  final box = await Hive.openBox<String>('templates');
  // check if 'templateList' key exists, if not, create a new list and save it to the box
  if (!box.containsKey('templateList')) {
    final List<TemplateModel> templateList = [];
    final jsonString = jsonEncode(templateList);
    await box.put('templateList', jsonString);
  }
  runApp(const MyApp());
  configLoading();
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthenticationBloc>(
      create: (context) => AuthenticationBloc(),
      child: MaterialApp.router(
        theme: ThemeData(fontFamily: GoogleFonts.openSans().fontFamily),
        debugShowCheckedModeBanner: false,
        // routerDelegate: initRoutes().routerDelegate,
        routerConfig: initRoutes(),
        builder: EasyLoading.init(),
      ),
    );
  }
}
