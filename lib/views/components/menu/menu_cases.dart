import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/controllers/templates_controller.dart';
import 'package:cms_templates/views/components/acc_sets/account/account.dart';
import 'package:cms_templates/views/components/acc_sets/settings/settings.dart';
import 'package:cms_templates/views/components/content/chart/chart.dart';
import 'package:cms_templates/views/components/content/users/users.dart';
import 'package:cms_templates/views/admin/not_found_page.dart';
import 'package:flutter/material.dart';

class MenuCases extends StatelessWidget {
  const MenuCases({Key? key, required this.menuItemState}) : super(key: key);

  // SelectMenuItem state from homepage
  final String menuItemState;

  /* [092301TREE]
  This widget used to handle the display of interface based on menu item
  */

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [_buildMenuItem(menuItemState)],
    );
  }

  Widget _buildMenuItem(String menuItem) {
    // For each menu item belongs to case then display widget corresponding
    switch (menuItem) {
      case templatesCase:
        return const TemplatesController();
      case chartCase:
        return const Chart();
      case usersCase:
        return const Users();
      case accountCase:
        return const Account();
      case settingsCase:
        return const Settings();
      default:
        return const NotFoundPage();
    }
  }
}
