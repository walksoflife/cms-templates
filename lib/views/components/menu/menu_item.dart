import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/helpers/custom_text.dart';
import 'package:flutter/material.dart';

class MenuItem extends StatelessWidget {
  const MenuItem(
      {super.key,
      required this.title,
      required this.icon,
      required this.press});

  final String title;
  final Icon icon;
  final VoidCallback press;

  /* [092301TREE]
  This widget receives parameters: (title, icon, press) to display the menu item
  - title: name of the menu item
  - icon: icon of the menu item
  - press: function to handle event press
  */

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4),
      child: ListTile(
        dense: true,
        leading: icon,
        title: customText(title, mediumSize, textNormal, kWhiteColor),
        onTap: press,
      ),
    );
  }
}
