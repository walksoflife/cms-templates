import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/helpers/custom_text.dart';
import 'package:flutter/material.dart';

class MenuHeader extends StatelessWidget {
  const MenuHeader({Key? key}) : super(key: key);

  /* [092301TREE]
  This widget includes two parts:
  - Logo of page
  - Name of page
  */

  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
      padding: const EdgeInsets.only(left: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              customText('Logo', largeSize, textBold, kWhiteColor),
              const Spacer(),
              customText(nameApp, largeSize, textBold, kWhiteColor),
            ],
          ),
        ],
      ),
    );
  }
}
