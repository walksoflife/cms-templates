import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/views/components/menu/menu_header.dart';
import 'package:cms_templates/views/components/menu/menu_item.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/bloc_cubit/menu/menu_cubit.dart';
import 'package:cms_templates/bloc_cubit/menu/menu_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Menu extends StatefulWidget {
  const Menu({Key? key, required this.menuState}) : super(key: key);

  // SelectMenuItem state from homepage
  final dynamic menuState;

  @override
  State<Menu> createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  // list menu items. each menu item includes two part: name and icon
  final List<Map<String, Icon>> menuList = [
    {templatesCase: const Icon(Icons.all_inbox, color: kWhiteColor)},
    {chartCase: const Icon(Icons.area_chart, color: kWhiteColor)},
    {usersCase: const Icon(Icons.person, color: kWhiteColor)}
  ];

  // [092301TREE]
  /*
  This widget includes two columns:
  - Column 1: MenuHeader that gets from file path 'lib/views/menu/menu_header.dart'
  - Column 2: list menu items
  */

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: kBlackColor,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        children: [
          // column 1: menu header
          const SizedBox(
            height: 100,
            child: MenuHeader(),
          ),
          // column 2: handle and display menu items
          Column(
            children: [
              /* [092301TREE]
              Loop through the menuList to get the name and icon to pass to buildMenuItem method,
              at the same time, pass context and menuState that is declared above
              - item.keys.first: name
              - item.values.first: icon
              */
              for (var item in menuList)
                buildMenuItem(item.keys.first, widget.menuState,
                    item.values.first, context),
            ],
          ),
          const Spacer(),
          // menu item: account
          buildMenuItem(accountCase, widget.menuState,
              const Icon(Icons.account_box, color: kWhiteColor), context),
          // menu item: settings
          buildMenuItem(settingsCase, widget.menuState,
              const Icon(Icons.settings, color: kWhiteColor), context),
        ],
      ),
    );
  }

  /* [092301TREE]
  This method receives parameters: (title, state, icon, context) used to:
  - Display menu item through MenuItem widget that gets from file path 'lib/views/menu/menu_item.dart'
  - highlight the current menu item selecting
  */
  Widget buildMenuItem(
      String title, MenuState state, Icon icon, BuildContext context) {
    // variable is used to store current menu item selecting
    String currentState = '';

    if (state is SelectMenuItem) {
      // update menu item selecting
      currentState = state.menuItem;
    }

    return Container(
      height: 50,
      decoration: BoxDecoration(
        // check if current state is title of menu item then highlight color of that menu item
        color: title == currentState ? kBlueColor : null,
        borderRadius: BorderRadius.circular(10),
      ),
      child: MenuItem(
        title: title,
        icon: icon,
        press: () {
          // get current state
          context.read<MenuCubit>().selectMenuItem(title);
        },
      ),
    );
  }
}
