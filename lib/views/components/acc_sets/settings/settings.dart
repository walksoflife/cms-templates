import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/constants/routes_text.dart';
import 'package:cms_templates/helpers/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class Settings extends StatelessWidget {
  const Settings({Key? key}) : super(key: key);

  /* [092301TREE]
  This widget includes:
  - Button logout is used to log out page
  */

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            ElevatedButton(
              onPressed: () {
                context.go(pathLoginPage);
              },
              child: Row(
                children: [
                  const Icon(Icons.logout_outlined,
                      color: kWhiteColor, size: 20),
                  const SizedBox(width: 8),
                  customText(logoutText, smallSize, textNormal, kWhiteColor),
                ],
              ),
            )
          ],
        )
      ],
    );
  }
}
