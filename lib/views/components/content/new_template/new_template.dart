import 'package:cms_templates/bloc_cubit/templates/templates_bloc.dart';
import 'package:cms_templates/bloc_cubit/templates/templates_event.dart';
import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/helpers/custom_text_button.dart';
import 'package:cms_templates/helpers/custom_text.dart';
import 'package:cms_templates/helpers/validate_form.dart';
import 'package:cms_templates/models/template_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class NewTemplate extends StatefulWidget {
  const NewTemplate({super.key, required this.bloc, required this.type});

  // get from Template widget
  final TemplateBloc bloc;
  // type may be 'add' or 'edit'
  final String type;

  @override
  State<NewTemplate> createState() => _NewTemplateState();
}

// this variable is used to store list type options
List<String> _typeOptions = [freeType, mediumType, masterType];

class _NewTemplateState extends State<NewTemplate> {
  // this variable is used to access states of from
  final _formField = GlobalKey<FormState>();
  // this variable is used to control text value of name field
  final TextEditingController _name = TextEditingController();
  // this variable is used to store current type selected
  String _typeCurrent = _typeOptions[0];

  /* [092301TREE]
  1.This widget is a alert dialog with content is a form and actions includes two buttons
  - Form has three fields: id, name, type
  - Two buttons: cancel and submit button
  2. This widget is used to create new template or edit template
  */

  // this function is used to get text input when update template event occurs
  String getTextInput(String field) {
    if (widget.type == editType) {
      switch (field) {
        case nameTemplate:
          return widget.bloc.state.name;
        case typeTemplate:
          return widget.bloc.state.type;
        default:
          return '';
      }
    }
    return '';
  }

  // function submit form
  void submitFormTemplate(BuildContext context) {
    final bloc = widget.bloc;
    // check if form validate successfully
    if (_formField.currentState!.validate()) {
      // new template
      final template = TemplateModel(
          id: bloc.repository.autoIncrementId(),
          name: _name.text,
          type: _typeCurrent);
      // add event add new template
      if (widget.type == editType) {
        bloc.add(UpdateTemplateEvent(template: template));
      } else
      // add event add new template
      if (widget.type == addType) {
        bloc.add(AddTemplateEvent(template: template));
      } else {
        return;
      }
      EasyLoading.showToast(getMsgSuccess(),
          toastPosition: EasyLoadingToastPosition.top);
    } else {
      EasyLoading.showToast(errorException,
          toastPosition: EasyLoadingToastPosition.top);
    }
  }

  // check type current button: 'Publish' for create or 'Save' for update
  String getTypeBtn() {
    if (widget.type == addType) {
      return addTemplateAction;
    } else if (widget.type == editType) {
      return editTemplateAction;
    }
    return '';
  }

  // check type current button: 'Publish' for create or 'Save' for update
  String getMsgSuccess() {
    if (widget.type == addType) {
      return addSuccess;
    } else if (widget.type == editType) {
      return updateSuccess;
    }
    return '';
  }

  @override
  void initState() {
    super.initState();
    _name.text = getTextInput(nameTemplate);
    _typeCurrent = getTextInput(typeTemplate);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      scrollable: true,
      title: customText(addTemplateTitle, largeSize, textBold, kBlackColor),
      content: SizedBox(
        width: 500,
        child: buildFormNewTemplate(),
      ),
      actions: [
        // cancel button
        customTextBtn(cancelText, () => Navigator.pop(context)),
        // submit button
        TextButton(
          style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(kTealColor),
          ),
          onPressed: () {
            submitFormTemplate(context);
            Navigator.pop(context);
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: customText(getTypeBtn(), smallSize, textNormal, kWhiteColor),
          ),
        )
      ],
    );
  }

  // this form is used to create template
  Form buildFormNewTemplate() {
    return Form(
      autovalidateMode: AutovalidateMode.always, // show error during validating
      key: _formField,
      child: Column(
        children: [
          // name field
          TextFormField(
            controller: _name,
            validator: (value) => Validator.validateNameTemplate(value),
            decoration: const InputDecoration(
              labelText: nameTemplate,
            ),
          ),
          const SizedBox(height: 15),
          // type field
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              customText(typeTemplate, mediumSize, textNormal, kBlackColor),
              ListTile(
                title:
                    customText(freeType, mediumSize, textNormal, kBlackColor),
                leading: Radio(
                  value: _typeOptions[0],
                  groupValue: _typeCurrent,
                  onChanged: (value) {
                    setState(() {
                      _typeCurrent = value.toString();
                    });
                  },
                ),
              ),
              ListTile(
                title:
                    customText(mediumType, mediumSize, textNormal, kBlackColor),
                leading: Radio(
                  value: _typeOptions[1],
                  groupValue: _typeCurrent,
                  onChanged: (value) {
                    setState(() {
                      _typeCurrent = value.toString();
                    });
                  },
                ),
              ),
              ListTile(
                title:
                    customText(masterType, mediumSize, textNormal, kBlackColor),
                leading: Radio(
                  value: _typeOptions[2],
                  groupValue: _typeCurrent,
                  onChanged: (value) {
                    setState(() {
                      _typeCurrent = value.toString();
                    });
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
