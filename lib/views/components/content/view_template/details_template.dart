import 'package:cms_templates/bloc_cubit/templates/templates_state.dart';
import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/helpers/custom_text_button.dart';
import 'package:cms_templates/helpers/custom_text.dart';
import 'package:flutter/material.dart';

class DetailsTemplate extends StatefulWidget {
  const DetailsTemplate({Key? key, required this.state}) : super(key: key);

  // get from
  final TemplateState state;

  /* [092305TREE]
  This widget is used to display details template includes: id, name, type
  */

  @override
  State<DetailsTemplate> createState() => _DetailsTemplateState();
}

class _DetailsTemplateState extends State<DetailsTemplate> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      scrollable: true,
      title: customText(detailTemplateTitle, largeSize, textBold, kBlackColor),
      content: SizedBox(
        width: 500,
        child: buildDetailTemplate(),
      ),
      actions: [
        customTextBtn(
            cancelText, () => Navigator.pop(context)), // cancel button
        // edit button
        // customTextBtn(editType, () {}),
        // // delete button
        // customTextBtn(deleteType, () {})
      ],
    );
  }

  // this form is used to create template
  Column buildDetailTemplate() {
    return Column(children: [
      Row(
        children: [
          customText('$idTemplate: ', mediumSize, textBold, kBlackColor),
          customText(
              widget.state.id.toString(), largeSize, textNormal, kBlackColor),
        ],
      ),
      const SizedBox(height: 15),
      Row(
        children: [
          customText('$nameTemplate: ', mediumSize, textBold, kBlackColor),
          customText(
              widget.state.name.toString(), largeSize, textNormal, kBlackColor),
        ],
      ),
      const SizedBox(height: 15),
      Row(
        children: [
          customText('$typeTemplate: ', mediumSize, textBold, kBlackColor),
          customText(
              widget.state.type.toString(), largeSize, textNormal, kBlackColor),
        ],
      ),
    ]);
  }
}
