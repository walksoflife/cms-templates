import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/helpers/custom_text.dart';
import 'package:flutter/material.dart';

class FilterTemplate extends StatefulWidget {
  const FilterTemplate({Key? key}) : super(key: key);

  @override
  State<FilterTemplate> createState() => _FilterTemplateState();
}

class _FilterTemplateState extends State<FilterTemplate> {
  // this variable is used to store default option
  late String selectOptDefault;
  // this variable is used to store all options
  List<String> options = ['All', 'Name', 'Price', 'Purchase'];

  /* [092301TREE]
  This widget is a drop down includes list options to filter templates
  */

  @override
  Widget build(BuildContext context) {
    // set selectOptDefault is the first of list options
    selectOptDefault = options.first;

    return Row(
      children: [
        customText(filterByText, mediumSize, textBold, kBlackColor),
        const SizedBox(width: 4),
        DropdownButton<String>(
          onChanged: (value) {},
          items: options
              .map(
                (String item) => DropdownMenuItem(
                  onTap: () {},
                  value: item,
                  child: customText(item, smallSize, textNormal, kBlackColor),
                ),
              )
              .toList(),
          value: selectOptDefault,
          dropdownColor: kWhiteColor,
        )
      ],
    );
  }
}
