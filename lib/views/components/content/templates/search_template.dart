import 'package:cms_templates/bloc_cubit/templates/templates_bloc.dart';
import 'package:cms_templates/bloc_cubit/templates/templates_event.dart';
import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:flutter/material.dart';

class SearchTemplate extends StatefulWidget {
  const SearchTemplate({Key? key, required this.bloc}) : super(key: key);

  // get from template widget
  final TemplateBloc bloc;

  @override
  State<SearchTemplate> createState() => _SearchTemplateState();
}

class _SearchTemplateState extends State<SearchTemplate> {
  // this variable is used to control input value user entered
  final TextEditingController _keyword = TextEditingController();

  /* [092305TREE]
  This widget is a form, include a input to enter keywords, search by name
  */
  
  // search function
  void _handleSearch() {
    final keyword = _keyword.text.toLowerCase();
    // search by name event
    widget.bloc.add(SearchTemplateEvent(keyword: keyword));
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      // key: _formField,
      child: Container(
        constraints: const BoxConstraints(maxWidth: 400),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              controller: _keyword,
              decoration: InputDecoration(
                filled: true,
                fillColor: kWhiteColor,
                contentPadding: const EdgeInsets.fromLTRB(20, 14, 20, 14),
                isCollapsed: true,
                hintText: hintTextSearch,
                suffixIcon: IconButton(
                  color: kBlueColor,
                  onPressed: () => _handleSearch(),
                  icon: const Icon(Icons.search, size: 20),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(largeRadius),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
