import 'package:cms_templates/bloc_cubit/templates/templates_bloc.dart';
import 'package:cms_templates/bloc_cubit/templates/templates_event.dart';
import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/helpers/custom_dialog.dart';
import 'package:cms_templates/helpers/custom_icon_button.dart';
import 'package:cms_templates/views/components/content/new_template/new_template.dart';
import 'package:cms_templates/views/components/content/templates/filter_templates.dart';
import 'package:cms_templates/views/components/content/templates/search_template.dart';
import 'package:cms_templates/views/components/content/templates/table_templates.dart';
import 'package:flutter/material.dart';

class Templates extends StatelessWidget {
  const Templates({Key? key, required this.bloc}) : super(key: key);

  // get from templateController
  final TemplateBloc bloc;

  /* [092301TREE]
  This widget is a column contains two parts:
  1. Row includes:
    - buildFormSearch method is used to search template
    - buildMethodTemplate method is used to add new template and delete many templates
    - FilterTemplate widget is used to filter template by fields
  2. TableTemplates widget
  */

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              // form search name template
              SearchTemplate(bloc: bloc),
              const Spacer(),

              // method template
              buildMethodTemplate(context, bloc),
              const SizedBox(width: 40),

              // filter template
              const FilterTemplate()
            ],
          ),
          const SizedBox(height: 20),

          // table data
          TableTemplates(bloc: bloc)
        ],
      ),
    );
  }

  /* [092301TREE]
  buildMethodTemplate includes two buttons:
  1. Add new template
  2. Delete many templates
  */
  Container buildMethodTemplate(BuildContext context, TemplateBloc bloc) {
    return Container(
      color: kWhiteColor,
      child: Row(
        children: [
          // add new template
          customIconBtn(addType, () {
            showDialog(
              context: context,
              builder: (context) => NewTemplate(bloc: bloc, type: addType),
            );
          }),
          // delete many templates
          customIconBtn(deleteType, () {
            customDialog(
              context,
              deleteTemplateTitle,
              deleteType,
              deleteManyTemplate,
              () {
                bloc.add(
                    DeleteTemplateEvent(templates: bloc.state.selectedList));
              },
            );
          }),
        ],
      ),
    );
  }
}
