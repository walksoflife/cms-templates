import 'package:cms_templates/bloc_cubit/templates/templates_bloc.dart';
import 'package:cms_templates/bloc_cubit/templates/templates_event.dart';
import 'package:cms_templates/bloc_cubit/templates/templates_state.dart';
import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/helpers/custom_dialog.dart';
import 'package:cms_templates/helpers/custom_icon_button.dart';
import 'package:cms_templates/helpers/custom_text.dart';
import 'package:cms_templates/helpers/sort_templates.dart';
import 'package:cms_templates/models/template_model.dart';
import 'package:cms_templates/views/components/content/new_template/new_template.dart';
import 'package:cms_templates/views/components/content/view_template/details_template.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class TableTemplates extends StatefulWidget {
  const TableTemplates({Key? key, required this.bloc}) : super(key: key);

  // get from templates widget
  final TemplateBloc bloc;

  @override
  State<TableTemplates> createState() => _TableTemplatesState();
}

class _TableTemplatesState extends State<TableTemplates> {
  /* [092305TREE]
  This widget is a table data
  - Form has three fields: name, type
  - Three buttons: view, edit and delete button
  */

  // function delete one template
  void deleteTemplate(List<TemplateModel> templates) {
    widget.bloc.add(DeleteTemplateEvent(templates: templates));
    if (!widget.bloc.state.isLoading) {
      EasyLoading.showToast(deleteSuccess,
          toastPosition: EasyLoadingToastPosition.top);
    }
    return;
  }

  // this variable is used to store current column sorting
  int _currentSortColumn = 0;
  // sort ascending false = descending
  bool _isSortAsc = false;
  // list template selected
  List<TemplateModel> _selectedList = [];

  @override
  Widget build(BuildContext context) {
    final bloc = widget.bloc; // get bloc template
    final state = widget.bloc.state; // get states from bloc template

    return Expanded(
      child: state.templates.isEmpty // templates is empty
          ? Center(
              child:
                  customText(noTemplates, largeSize, textNormal, kWhiteColor))
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                DataTable(
                  columns: _createColumns(state),
                  rows: _createRows(state, bloc),
                  sortColumnIndex: _currentSortColumn,
                  sortAscending: _isSortAsc,
                  dividerThickness: 1,
                  dataRowMaxHeight: 50,
                  // handle color of row when selected
                  dataRowColor: MaterialStateProperty.resolveWith((states) {
                    if (states.contains(MaterialState.selected)) {
                      return kBlueColor.withOpacity(0.3);
                    }
                    return kWhiteColor;
                  }),
                  // add heading color title
                  headingRowColor: MaterialStateProperty.resolveWith(
                      (states) => kWhiteColor),
                  showCheckboxColumn: true,
                  onSelectAll: (isSelectedAll) {
                    setState(() =>
                        _selectedList = isSelectedAll! ? state.templates : []);
                    bloc.add(SelectedTemplateEvent(templates: _selectedList));
                  },
                ),

                // pagination page
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    // previous page button
                    ElevatedButton(
                        onPressed: () => bloc.add(PreviousPageEvent()),
                        child: const Icon(Icons.chevron_left)),
                    const SizedBox(width: 14),
                    // display 'current page / totalPage'
                    Container(
                      width: 50,
                      alignment: Alignment.center,
                      child: customText(
                          '${state.currentPage} / ${state.totalPage}',
                          smallSize,
                          textBold,
                          kWhiteColor),
                    ),
                    const SizedBox(width: 14),
                    // next page button
                    ElevatedButton(
                        onPressed: () => bloc.add(NextPageEvent()),
                        child: const Icon(Icons.chevron_right)),
                  ],
                )
              ],
            ),
    );
  }

  // title table
  List<DataColumn> _createColumns(TemplateState state) {
    return [
      // id title
      DataColumn(
        label: customText(idTemplate, mediumSize, textBold, kBlackColor),
        onSort: (columnIndex, ascending) {
          // sort by id of current page
          sortTemplatesById(state.templates, ascending);
          setState(() {
            _currentSortColumn = columnIndex; // current column sorting
            _isSortAsc = ascending; // true or false
          });
        },
      ),
      // name title
      DataColumn(
        label: customText(nameTemplate, mediumSize, textBold, kBlackColor),
        onSort: (columnIndex, ascending) {
          // sort by name of current page
          sortTemplatesByName(state.templates, ascending);
          setState(() {
            _currentSortColumn = columnIndex; // current column sorting
            _isSortAsc = ascending; // true or false
          });
        },
      ),
      // type title
      DataColumn(
        label: customText(typeTemplate, mediumSize, textBold, kBlackColor),
      ),
      // fake empty string
      DataColumn(label: customText('', mediumSize, textBold, kBlackColor))
    ];
  }

  // row table
  List<DataRow> _createRows(TemplateState state, TemplateBloc bloc) {
    List<DataRow> rows = [];
    for (int index = 0; index < state.templates.length; index++) {
      TemplateModel template = state.templates[index];
      rows.add(
        DataRow(
          selected: _selectedList.contains(template),
          onSelectChanged: (isSelected) {
            setState(() {
              // check template is selecting
              final isAdding = isSelected != null && isSelected;
              // true then add to list selected, else remove out list
              isAdding
                  ? _selectedList.add(template)
                  : _selectedList.remove(template);
            });
            // set state for selectedList
            bloc.add(SelectedTemplateEvent(templates: _selectedList));
          },
          cells: [
            DataCell(Text(template.id.toString())),
            DataCell(Text(template.name)),
            DataCell(Text(template.type)),
            DataCell(Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // visible button
                customIconBtn(visibleType, () {
                  widget.bloc
                      .add(SpecificTemplateEvent(templateId: template.id));
                  showDialog(
                    context: context,
                    builder: (context) =>
                        DetailsTemplate(state: widget.bloc.state),
                  );
                }),
                // edit button
                customIconBtn(editType, () {
                  widget.bloc
                      .add(SpecificTemplateEvent(templateId: template.id));
                  showDialog(
                    context: context,
                    builder: (context) =>
                        NewTemplate(bloc: widget.bloc, type: editType),
                  );
                }),
                // delete button
                customIconBtn(deleteType, () {
                  customDialog(context, deleteTemplateTitle, deleteType,
                      deleteTemplateDesc, () {
                    // delete one but give it to list to
                    final List<TemplateModel> listRemove = [template];
                    deleteTemplate(listRemove);
                  });
                })
              ],
            ))
          ],
        ),
      );
    }
    return rows;
  }
}
