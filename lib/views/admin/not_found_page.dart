import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/constants/routes_text.dart';
import 'package:cms_templates/helpers/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class NotFoundPage extends StatelessWidget {
  const NotFoundPage({Key? key}) : super(key: key);

  /* [092301TREE]
  This widget is a page that displays error 404 not found
  */
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 100),
          child: Column(
            children: [
              customText(notFoundText, bigSize, textBold, kBlackColor),
              const SizedBox(height: 20),
              customText(errNotFoundText, mediumSize, textNormal, kBlackColor),
              const SizedBox(height: 20),
              // button go back to home page
              ElevatedButton(
                onPressed: () => context.go(pathHomePage),
                child: customText(
                    suggestNotFoundText, smallSize, textNormal, kWhiteColor),
              )
            ],
          ),
        ),
      ),
    );
  }
}
