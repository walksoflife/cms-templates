import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/constants/routes_text.dart';
import 'package:cms_templates/helpers/custom_text.dart';
import 'package:cms_templates/helpers/validate_form.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:cms_templates/constants/index.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formField = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  bool passwordToggle = true;

  void submitLoginForm(BuildContext context) {
    if (_formField.currentState!.validate()) {
      context.go(pathHomePage);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Form(
          key: _formField,
          child: Container(
            constraints: const BoxConstraints(maxWidth: 400),
            padding: const EdgeInsets.all(24),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // text information
                Row(children: [
                  customText(loginTxt, bigSize, textBold, kBlackColor)
                ]),
                const SizedBox(height: 10),
                Row(children: [
                  customText(welcomeTxt, smallSize, textNormal, kBlackColor)
                ]),
                const SizedBox(height: 15),

                // input email
                buildTextFieldEmail(),
                const SizedBox(height: 15),

                // input password
                buildTextFieldPassword(),

                const SizedBox(height: 15),

                // text information
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Checkbox(value: true, onChanged: (value) {}),
                        customText(
                            rememberMeTxt, smallSize, textNormal, kGrayColor)
                      ],
                    ),
                    customText(forgotPwdTxt, smallSize, textNormal, kBlueColor)
                  ],
                ),

                const SizedBox(height: 15),

                // button submit form
                buildButtonSubmit(context),

                const SizedBox(height: 15),

                // text information
                buildTextInformation()
              ],
            ),
          ),
        ),
      ),
    );
  }

  TextFormField buildTextFieldPassword() {
    return TextFormField(
      controller: passwordController,
      obscureText: passwordToggle,
      validator: (value) => Validator.validatePassword(value),
      decoration: InputDecoration(
          labelText: labelPwdText,
          hintText: hintTextPwd,
          prefixIcon: const Icon(Icons.lock, size: 18),
          suffixIcon: InkWell(
              onTap: () {
                setState(() {
                  passwordToggle = !passwordToggle;
                });
              },
              child: Icon(
                  passwordToggle ? Icons.visibility : Icons.visibility_off)),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(largeRadius))),
    );
  }

  TextFormField buildTextFieldEmail() {
    return TextFormField(
      controller: emailController,
      validator: (value) => Validator.validateEmail(value),
      decoration: InputDecoration(
          labelText: labelEmailTxt,
          hintText: hintTextEmail,
          prefixIcon: const Icon(Icons.email, size: 18),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(largeRadius))),
    );
  }

  InkWell buildButtonSubmit(BuildContext context) {
    return InkWell(
      onTap: () => submitLoginForm(context),
      child: Container(
          decoration: BoxDecoration(
              color: kBlueColor,
              borderRadius: BorderRadius.circular(largeRadius)),
          alignment: Alignment.center,
          width: double.maxFinite,
          padding: const EdgeInsets.symmetric(vertical: 16),
          child: customText(loginTxt, mediumSize, textNormal, kWhiteColor)),
    );
  }

  Container buildTextInformation() {
    return Container(
      child: RichText(
          text: const TextSpan(children: [
        TextSpan(text: credentialText, style: TextStyle(color: kGrayColor)),
        TextSpan(text: reqCredentialTxt, style: TextStyle(color: kBlueColor))
      ])),
    );
  }
}
