import 'package:cms_templates/bloc_cubit/menu/menu_cubit.dart';
import 'package:cms_templates/bloc_cubit/menu/menu_state.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/views/components/menu/menu.dart';
import 'package:cms_templates/views/components/menu/menu_cases.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  // [092301TREE]
  /*
  This widget is wrapped by MenuCubit, includes two columns:
  - Column 1: Menu that gets from file path 'lib/views/components/menu/menu.dart'
  - Column 2: MenuCases that is used to handle the display of interface based on menu item
  */

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        padding: const EdgeInsets.all(20),
        child: BlocProvider(
          create: (context) => MenuCubit(),
          child: BlocBuilder<MenuCubit, MenuState>(
            builder: (context, state) {
              if (state is SelectMenuItem) {
                // render views
                return Row(
                  children: [
                    // widget menu receives a state parameter that is a SelectMenuItem state
                    Expanded(
                      flex: 2,
                      child: Menu(menuState: state),
                    ),
                    const SizedBox(width: 20),
                    // view of each menu item
                    Expanded(
                      flex: 6,
                      child: Container(
                        padding: const EdgeInsets.all(20),
                        decoration: const BoxDecoration(
                          color: kGrayColor,
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: MenuCases(menuItemState: state.menuItem),
                      ),
                    )
                  ],
                );
              } else {
                // nothing
                return Container();
              }
            },
          ),
        ),
      ),
    );
  }
}
