/* [092301TREE]
Fake templates data
*/
class DataApp {
  static final templates = {
    "templates": [
      {"id": 1, "name": "Painted stork", "type": "free"},
      {"id": 2, "name": "Tortoise black mountain", "type": "medium"},
      {"id": 3, "name": "Southern right whale", "type": "master"},
      {"id": 4, "name": "Opossum, american virginia", "type": "master"},
      {"id": 5, "name": "Chilean flamingo", "type": "master"},
      {"id": 6, "name": "Egyptian vulture", "type": "master"},
      {"id": 7, "name": "Yellow-billed hornbill", "type": "medium"},
      {"id": 8, "name": "Kafue flats lechwe", "type": "medium"},
      {"id": 9, "name": "Cobra (unidentified)", "type": "free"},
      {"id": 10, "name": "North American beaver", "type": "medium"},
      {"id": 11, "name": "Gnu, brindled", "type": "master"},
      {"id": 12, "name": "Lourie, grey", "type": "free"},
      {"id": 13, "name": "Snowy egret", "type": "free"},
      {"id": 14, "name": "Common rhea", "type": "medium"},
      {"id": 15, "name": "Heron, giant", "type": "medium"},
      {"id": 16, "name": "Red-billed toucan", "type": "medium"},
      {"id": 17, "name": "Brolga crane", "type": "free"},
    ]
  };
}
