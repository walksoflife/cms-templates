import 'package:cms_templates/bloc_cubit/templates/templates_bloc.dart';
import 'package:cms_templates/bloc_cubit/templates/templates_event.dart';
import 'package:cms_templates/bloc_cubit/templates/templates_state.dart';
import 'package:cms_templates/constants/index.dart';
import 'package:cms_templates/helpers/custom_text.dart';
import 'package:cms_templates/repo/templates/repo_templates.dart';
import 'package:cms_templates/views/components/content/templates/templates.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TemplatesController extends StatelessWidget {
  const TemplatesController({Key? key}) : super(key: key);

  // [092301TREE]

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TemplateBloc(
        repository: TemplateRepository(),
      )..add(TemplateFetchEvent()),
      child: buildListTemplates(),
    );
  }

  BlocBuilder<TemplateBloc, TemplateState> buildListTemplates() {
    return BlocBuilder<TemplateBloc, TemplateState>(
      builder: (context, state) {
        switch (state.status) {
          // if status is initial then show loading
          case TemplateStatus.initial:
            return const Expanded(
                child: Center(child: CircularProgressIndicator()));
          // if status is failure then show error text
          case TemplateStatus.failure:
            return Center(
              child: customText(state.error, bigSize, textNormal, kRedColor),
            );
          // if status is success then get templates
          case TemplateStatus.success:
            // this variable is used to store bloc templates state
            final bloc = context.read<TemplateBloc>();

            // props through Templates widget
            if (state.isLoading) {
              return const Expanded(
                  child: Center(child: CircularProgressIndicator()));
            }
            return Templates(bloc: bloc);
          default:
            return Container();
        }
      },
    );
  }
}
