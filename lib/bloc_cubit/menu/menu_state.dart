import 'package:equatable/equatable.dart';

abstract class MenuState extends Equatable {}

class SelectMenuItem extends MenuState {
  final String menuItem;

  SelectMenuItem({required this.menuItem});

  @override
  List<Object> get props => [menuItem];
}
