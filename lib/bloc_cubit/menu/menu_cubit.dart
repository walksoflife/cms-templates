import 'package:cms_templates/bloc_cubit/menu/menu_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MenuCubit extends Cubit<MenuState> {
  MenuCubit() : super(SelectMenuItem(menuItem: 'Templates'));

  void selectMenuItem(String menuItem) {
    emit(SelectMenuItem(menuItem: menuItem));
  }
}
