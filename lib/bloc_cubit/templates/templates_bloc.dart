import 'package:cms_templates/bloc_cubit/templates/templates_event.dart';
import 'package:cms_templates/bloc_cubit/templates/templates_state.dart';
import 'package:cms_templates/constants/app_text.dart';
import 'package:cms_templates/repo/templates/repo_templates.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TemplateBloc extends Bloc<TemplateEvent, TemplateState> {
  final TemplateRepository repository;

  // [092302TREE]

  TemplateBloc({required this.repository}) : super(const TemplateState()) {
    on<TemplateFetchEvent>(_onTemplateFetch);
    on<AddTemplateEvent>(_onAddTemplate);
    on<UpdateTemplateEvent>(_onUpdateTemplate);
    on<DeleteTemplateEvent>(_onDeleteTemplate);
    on<SpecificTemplateEvent>(_onSpecificTemplate);
    on<PreviousPageEvent>(_onPreviousPage);
    on<NextPageEvent>(_onNextPage);
    on<SearchTemplateEvent>(_onSearchTemplate);
    on<SelectedTemplateEvent>(_onSelectedTemplate);
  }

  // fetch templates event
  void _onTemplateFetch(
      TemplateFetchEvent event, Emitter<TemplateState> emit) async {
    try {
      // if status is initial
      if (state.status == TemplateStatus.initial) {
        // loading state
        emit(state.copyWith(isLoading: true));
        // get list converted
        final listConverted = repository.getListConverted();
        // check list current in box is empty , true -> fetch 17 items
        if (listConverted.isEmpty) {
          await repository.getAllTemplates();
        }
        // get templates with page equal current page = 0 in initial status
        final templates =
            await repository.getTemplatesFromBox(state.currentPage);
        // update states
        emit(state.copyWith(
            status: TemplateStatus.success,
            templates: templates,
            isLoading: false,
            totalPage: repository.calculateTotalPage()));
      }
    } catch (error) {
      // got error
      emit(state.copyWith(
          status: TemplateStatus.failure,
          isLoading: false,
          error: error.toString()));
    }
  }

  // add new template event
  void _onAddTemplate(
      AddTemplateEvent event, Emitter<TemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // add new template to box
      await repository.addOrEditTemplateOfBox(event.template, addType);
      // get templates after adding new template
      final templates = await repository.getTemplatesFromBox(state.currentPage);
      // delayed loading during one second
      await Future.delayed(const Duration(seconds: 1));
      // update states
      emit(state.copyWith(
          status: TemplateStatus.success,
          templates: templates.take(10).toList(),
          isLoading: false,
          totalPage: repository.calculateTotalPage()));
    } catch (_) {
      emit(state.copyWith(status: TemplateStatus.failure, isLoading: false));
    }
  }

  // update template event
  void _onUpdateTemplate(
      UpdateTemplateEvent event, Emitter<TemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // edit template to box
      await repository.addOrEditTemplateOfBox(event.template, editType);
      // get templates after updating
      final templates = await repository.getTemplatesFromBox(state.currentPage);
      // delayed loading during one second
      await Future.delayed(const Duration(seconds: 1));
      // update states
      emit(state.copyWith(
          status: TemplateStatus.success,
          templates: templates,
          isLoading: false,
          totalPage: repository.calculateTotalPage()));
    } catch (_) {
      emit(state.copyWith(status: TemplateStatus.failure, isLoading: false));
    }
  }

  // delete template event
  void _onDeleteTemplate(
      DeleteTemplateEvent event, Emitter<TemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // delete one, many templates
      await repository.deleteTemplate(event.templates);
      // get templates after deleting
      final templates = await repository.getTemplatesFromBox(state.currentPage);
      // delayed loading during one second
      await Future.delayed(const Duration(seconds: 1));
      // update states
      emit(state.copyWith(
          status: TemplateStatus.success,
          templates: templates,
          isLoading: false,
          totalPage: repository.calculateTotalPage()));
    } catch (_) {
      emit(state.copyWith(status: TemplateStatus.failure, isLoading: false));
    }
  }

  // delete template event
  void _onSpecificTemplate(
      SpecificTemplateEvent event, Emitter<TemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // get one template with template id
      final template = repository.getOneTemplate(event.templateId);
      // update states
      emit(state.copyWith(
          status: TemplateStatus.success,
          id: template.id,
          name: template.name,
          type: template.type,
          isLoading: false));
    } catch (_) {
      emit(state.copyWith(status: TemplateStatus.failure, isLoading: false));
    }
  }

  // previous page event
  void _onPreviousPage(
      PreviousPageEvent event, Emitter<TemplateState> emit) async {
    // check currentPage state must > 1 then allow to go previous page
    if (state.currentPage > 1) {
      // loading state
      emit(state.copyWith(isLoading: true));
      // when click nextPage button, page will be decrease 1
      final page = state.currentPage - 1;
      try {
        // get templates with page equal page variable has been declared above
        final templates = await repository.getTemplatesFromBox(page);
        // delayed loading during 1 second
        await Future.delayed(const Duration(seconds: 1));
        // update states
        emit(state.copyWith(
            currentPage: page,
            status: TemplateStatus.success,
            templates: templates,
            isLoading: false));
      } catch (_) {
        // got error
        emit(state.copyWith(status: TemplateStatus.failure, isLoading: false));
      }
    }
    return;
  }

  // next page event
  void _onNextPage(NextPageEvent event, Emitter<TemplateState> emit) async {
    // check two conditions must satisfy then allow to go next page
    if (state.currentPage + 1 <= repository.calculateTotalPage()) {
      // loading state
      emit(state.copyWith(isLoading: true));
      // when click nextPage button, page will be increase 1
      final page = state.currentPage + 1;
      try {
        // get templates with page equal page variable has been declared above
        final templates = await repository.getTemplatesFromBox(page);
        // delayed loading during 1 second
        await Future.delayed(const Duration(seconds: 1));
        // update states
        emit(state.copyWith(
            currentPage: page,
            status: TemplateStatus.success,
            templates: templates,
            isLoading: false));
      } catch (_) {
        // got error
        emit(state.copyWith(status: TemplateStatus.failure, isLoading: false));
      }
    }
    return;
  }

  // search template event
  void _onSearchTemplate(
      SearchTemplateEvent event, Emitter<TemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // search template with keyword
      final listSearch = await repository.searchTemplates(event.keyword);
      // get templates after searching
      // final templates =
      // await repository.getTemplatesFromBox(state.currentPage, listSearch);
      // delayed loading during one second
      await Future.delayed(const Duration(seconds: 1));
      // update states
      emit(state.copyWith(
          status: TemplateStatus.success,
          templates: listSearch,
          isLoading: false,
          totalPage: repository.calculateTotalPage()));
    } catch (_) {
      emit(state.copyWith(status: TemplateStatus.failure, isLoading: false));
    }
  }

  // selected templates event
  void _onSelectedTemplate(
      SelectedTemplateEvent event, Emitter<TemplateState> emit) async {
    emit(state.copyWith(
      status: TemplateStatus.success,
      selectedList: event.templates,
      isLoading: false,
    ));
  }
}
