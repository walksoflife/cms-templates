import 'package:cms_templates/models/template_model.dart';
import 'package:equatable/equatable.dart';

// [092302TREE]

enum TemplateStatus { initial, success, failure }

class TemplateState extends Equatable {
  final TemplateStatus status;
  final List<TemplateModel> templates;
  final List<TemplateModel> selectedList;
  final bool hasLoadedAll;
  final int currentPage;
  final bool isLoading;
  final int totalPage;
  final String error;
  final int id;
  final String name;
  final String type;

  const TemplateState({
    this.status = TemplateStatus.initial,
    this.templates = const <TemplateModel>[],
    this.selectedList = const <TemplateModel>[],
    this.hasLoadedAll = false,
    this.currentPage = 1,
    this.isLoading = false,
    this.totalPage = 0,
    this.error = '',
    this.id = 1,
    this.name = '',
    this.type = '',
  });

  TemplateState copyWith({
    TemplateStatus? status,
    List<TemplateModel>? templates,
    List<TemplateModel>? selectedList,
    bool? hasLoadedAll,
    int? currentPage,
    bool? isLoading,
    int? totalPage,
    String? error,
    int? id,
    String? name,
    String? type,
  }) {
    return TemplateState(
      status: status ?? this.status,
      templates: templates ?? this.templates,
      selectedList: selectedList ?? this.selectedList,
      hasLoadedAll: hasLoadedAll ?? this.hasLoadedAll,
      currentPage: currentPage ?? this.currentPage,
      isLoading: isLoading ?? this.isLoading,
      totalPage: totalPage ?? this.totalPage,
      error: error ?? this.error,
      id: id ?? this.id,
      name: name ?? this.name,
      type: type ?? this.type,
    );
  }

  @override
  List<Object> get props => [
        status,
        templates,
        hasLoadedAll,
        currentPage,
        isLoading,
        totalPage,
        error,
        id,
        name,
        type,
        selectedList
      ];
}
