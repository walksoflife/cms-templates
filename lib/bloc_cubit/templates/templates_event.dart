import 'package:cms_templates/models/template_model.dart';
import 'package:equatable/equatable.dart';

// [092302TREE]

abstract class TemplateEvent extends Equatable {
  @override
  List<Object> get props => [];
}

// fetch all templates
class TemplateFetchEvent extends TemplateEvent {}

// fetch previous page
class PreviousPageEvent extends TemplateEvent {}

// fetch next page
class NextPageEvent extends TemplateEvent {}

// fetch single page
class SpecificTemplateEvent extends TemplateEvent {
  final int templateId;

  SpecificTemplateEvent({required this.templateId});

  @override
  List<Object> get props => [templateId];
}

// new template
class AddTemplateEvent extends TemplateEvent {
  final TemplateModel template;

  AddTemplateEvent({required this.template});

  @override
  List<Object> get props => [template];
}

// update template
class UpdateTemplateEvent extends TemplateEvent {
  final TemplateModel template;

  UpdateTemplateEvent({required this.template});

  @override
  List<Object> get props => [template];
}

// delete template
class DeleteTemplateEvent extends TemplateEvent {
  final List<TemplateModel> templates;

  DeleteTemplateEvent({required this.templates});

  @override
  List<Object> get props => [templates];
}

// search template
class SearchTemplateEvent extends TemplateEvent {
  final String keyword;

  SearchTemplateEvent({required this.keyword});

  @override
  List<Object> get props => [keyword];
}

// selected templates changed
class SelectedTemplateEvent extends TemplateEvent {
  final List<TemplateModel> templates;

  SelectedTemplateEvent({required this.templates});

  @override
  List<Object> get props => [templates];
}
