import 'package:cms_templates/bloc_cubit/auth/auth_event.dart';
import 'package:cms_templates/bloc_cubit/auth/auth_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc() : super(const AuthenticationState()) {
    on<LoginEvent>(_onLoginPage);
    on<LogoutEvent>(_onLogoutPage);
  }

  // login page event
  void _onLoginPage(LoginEvent event, Emitter<AuthenticationState> emit) async {
    emit(state.copyWith(isLoading: true));
    try {
      emit(state.copyWith(
          status: AuthenticationStatus.success,
          isLoading: false,
          isAuthenticated: true));
    } catch (error) {
      // got error
      emit(state.copyWith(
          status: AuthenticationStatus.failure,
          isLoading: false,
          error: error.toString()));
    }
  }

  // logout page event
  void _onLogoutPage(
      LogoutEvent event, Emitter<AuthenticationState> emit) async {
    emit(state.copyWith(isLoading: true));
    try {
      emit(state.copyWith(
          status: AuthenticationStatus.success,
          isLoading: false,
          isAuthenticated: false));
    } catch (error) {
      // got error
      emit(state.copyWith(
          status: AuthenticationStatus.failure,
          isLoading: false,
          error: error.toString()));
    }
  }
}
