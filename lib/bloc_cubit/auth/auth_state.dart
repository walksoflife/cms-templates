import 'package:equatable/equatable.dart';

enum AuthenticationStatus { initial, success, failure }

class AuthenticationState extends Equatable {
  final bool isAuthenticated;
  final AuthenticationStatus status;
  final bool isLoading;
  final String error;

  const AuthenticationState(
      {this.status = AuthenticationStatus.initial,
      this.isAuthenticated = false,
      this.isLoading = false,
      this.error = ''});

  AuthenticationState copyWith(
      {AuthenticationStatus? status,
      bool? isAuthenticated,
      bool? isLoading,
      String? error}) {
    return AuthenticationState(
        status: status ?? this.status,
        isAuthenticated: isAuthenticated ?? this.isAuthenticated,
        isLoading: isLoading ?? this.isLoading,
        error: error ?? this.error);
  }

  @override
  List<Object> get props => [status, isAuthenticated, isLoading, error];
}
